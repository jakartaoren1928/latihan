package com.projek.demo.repository;

import java.util.List;

import com.projek.demo.models.Tutorial;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TutorialRepository extends JpaRepository<Tutorial, Long>{
    List<Tutorial> findByPublished(boolean published);

  List<Tutorial> findByTitleContaining(String title);
  
  //dari model
  @Query("Select p from Tutorial p where p.description=:description")
  List<Tutorial> getBydesc (@Param("description")String description);
  
  //dari database
  @Query(value="SELECT * FROM TUTORIALS P WHERE P.description=:description",nativeQuery = true)
  List<Tutorial> getBytitle (@Param("description")String description);

  @Query(value="SELECT id FROM TUTORIALS P WHERE P.description=:description",nativeQuery = true)
  Integer getByID (@Param("description")String description);

}
